public class MyLib {

	private final static String LIB_NAME = "MyLib";

	static  {
		try {
			System.out.printf("Loading '%s'%n", LIB_NAME);
			System.loadLibrary(LIB_NAME);			
			System.out.printf("Loaded '%s'%n", LIB_NAME);
		} catch (Exception e) {
			System.err.printf("Exception loading \"%s\" Exception: %s%n",  LIB_NAME, e.getMessage());
			e.printStackTrace(System.out);
		}
	}

	private native static int native_add(int a, int b);
		
	public static void main(String[] args) {
		System.out.printf("Start%n");
		System.out.printf("add(51700, 73) = %d%n", native_add(51700, 73));
		System.out.printf("End%n");
	}
}
