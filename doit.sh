#!/bin/bash

	echo "=== Building on OS-X target on OS-X =="

	export JAVA_HOME="`/usr/libexec/java_home -v '14*'`"
	
	# Clean up
	rm -rf bin .class MyLib/*.o *.dll *.jnilib *.jar Main Main.exe winlibs
	
	# Compile Java and generate JNI header file
	mkdir -p bin
	javac -d bin -h MyLib -sourcepath src src/*.java

	# Create the JAR file, main entry point in class MyLib
	jar cfe Test.jar MyLib -C bin .
	
	# Build the C++ shared library
	gcc -c -I"${JAVA_HOME}/include" -I"${JAVA_HOME}/include/darwin" MyLib/MyLib.cpp -o MyLib/MyLib.mac.o
	g++ -dynamiclib -o libMyLib.jnilib MyLib/*.mac.o
	
	echo "=== Running on OS-X ==="
	java -jar Test.jar

	echo "=== Building Windows target on OS-X ==="
	
	# Cross compile for the Windows shared library
	/usr/local/bin/x86_64-w64-mingw32-gcc -D __LP64__ -c -I$JAVA_HOME/include -I$JAVA_HOME/include/darwin MyLib/MyLib.cpp -o MyLib/MyLib.win.o
	/usr/local/bin/x86_64-w64-mingw32-g++ -shared -static -o MyLib.dll MyLib/*.win.o

	echo "=== Building C++ Main App on OS-X for target on OS-X ==="
	g++ -c -I"${JAVA_HOME}/include" -I"${JAVA_HOME}/include/darwin"  MyLib/Main.cpp -o MyLib/Main.mac.o
	g++ -o Main libMyLib.jnilib MyLib/Main.mac.o
	
	echo "=== Building C++ Main App on OS-X for target on Windows ==="
	/usr/local/bin/x86_64-w64-mingw32-gcc -D __LP64__ -c -I$JAVA_HOME/include -I$JAVA_HOME/include/darwin MyLib/Main.cpp -o MyLib/Main.win.o
	/usr/local/bin/x86_64-w64-mingw32-g++ -o Main.exe MyLib.dll MyLib/Main.win.o
	
	echo "=== THE END ==="
