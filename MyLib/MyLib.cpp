#include <jni.h>
#include "MyLib.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Class:     MyLib
 * Method:    native_add
 * Signature: (II)I
 */
JNIEXPORT jint JNICALL Java_MyLib_native_1add
  (JNIEnv *env, jclass, jint a, jint b) {
  	int *someInts = new int[3]; // Comment out this line and it works on Windows
  	return a + b;
}

#ifdef __cplusplus
}
#endif
